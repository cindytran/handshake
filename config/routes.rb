Rails.application.routes.draw do
  get '/index', to: "users#index"

  resource :user
end
