# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
user_1 = User.create!(first_name: 'Mickey', last_name: 'Mouse', check_in_time: Date.new(2022,1,2))
user_2 = User.create!(first_name: 'Daisy', last_name: 'Duck', check_in_time: Date.new(2021,3,4))
