require 'rails_helper'

RSpec.describe "Users", type: :request do
  describe "GET /index" do
    before do
      user_1 = User.create(first_name: "Mickey", last_name: "Mouse",
        check_in_time: Date.new(1999,1,1))
      user_2 = User.create(first_name: "Minnie", last_name: "Mouse",
        check_in_time: Date.new(2001,2,2))

      @users = [user_1, user_2]
    end

    it 'returns the list of users' do
      get "/"

      expect(response).to have_http_status(:ok)

      @users.each do |user|
        expect(response.body).to match("<td>#{user.first_name}</td>")
        expect(response.body).to match("<td>#{user.last_name}</td>")
        expect(response.body).to match("<td>#{user.check_in_time}</td>")
      end
    end
  end
end
